import React from 'react';
import $ from 'jquery';

class MenuBar extends React.Component{
    constructor(props){
        super(props);
        this.game = props.game;
        this.game.AddEventListener('state_change',this.stateChangeCallback.bind(this));
        this.state = {
            ingame:false
        }
    }
    stateChangeCallback(){
        this.setState({
            ...this.state,
            ingame:this.game.state===2
        });
    }
    handleLogout(e){
        e.preventDefault();
        this.game.Logout();
        return false;
    }
    changePageToLounge(e){
        e.preventDefault();
        this.game.ChangePage('lounge');
        return false;
    }
    changePageToServices(e){
        e.preventDefault();
        this.game.ChangePage('services');
        return false;
    }
    changePageToSales(e){
        e.preventDefault();
        this.game.ChangePage('sales');
        return false;
    }
    changePageToProduction(e){
        e.preventDefault();
        this.game.ChangePage('production');
        return false;
    }
    changePageToInvestiments(e){
        e.preventDefault();
        this.game.ChangePage('investiments');
        return false;
    }
    changePageToMyRooms(e){
        e.preventDefault();
        this.game.ChangePage('admin_lounge');
        return false;
    }
    changePageToMatch(e){
        e.preventDefault();
        this.game.ChangePage('admin_match');
        return false;
    }
    render(){
        if(this.game.user.bond==='Teacher'){
            return (<div id="dock-container">
                <ul>
                    <li key="1" onClick={this.changePageToMyRooms.bind(this)}>
                        <div><i className="fas fa-list"></i></div>
                        <span>Lobby</span>
                    </li>
                    <li key="2" onClick={this.changePageToMatch.bind(this)}>
                        <div><i className="fas fa-table"></i></div>
                        <span>Partida</span>
                    </li>
                    <li key="3" onClick={this.handleLogout.bind(this)}>
                        <div><i className="fas fa-sign-out-alt sair"></i></div>
                        <span className="sair">Logout</span>
                    </li>
                </ul>
            </div>);
        }
        else{
            if(!this.state.ingame){
                return (
                    <div id="dock-container">
                        <ul>
                            <li key="6" onClick={this.handleLogout.bind(this)}>
                                <div><i className="fas fa-sign-out-alt sair"></i></div>
                                <span className="sair">Logout</span>
                            </li>
                        </ul>
                    </div>
                );
            }
            else{
                return (
                    <div id="dock-container">
                        <ul>
                            <li key="1" onClick={this.changePageToLounge.bind(this)}>
                                <div><i className="fas fa-couch"></i></div>
                                <span>Jornal</span>
                            </li>
                            <li key="2" onClick={this.changePageToServices.bind(this)}>
                                <div><i className="fas fa-truck"></i></div>
                                <span>Serviços</span>
                            </li>
                            <li key="3" onClick={this.changePageToSales.bind(this)}>
                                <div><i className="fas fa-shopping-cart"></i></div>
                                <span>Vendas</span>
                            </li>
                            <li key="4" onClick={this.changePageToProduction.bind(this)}>
                                <div><i className="fas fa-industry"></i></div>
                                <span>Produção</span>
                            </li>
                            <li key="5" onClick={this.changePageToInvestiments.bind(this)}>
                                <div><i className="fas fa-hand-holding-usd"></i></div>
                                <span>Investimento</span>
                            </li>
                            <li key="6" onClick={this.handleLogout.bind(this)}>
                                <div><i className="fas fa-sign-out-alt sair"></i></div>
                                <span className="sair">Sair</span>
                            </li>
                        </ul>
                    </div>
                );
            }
        }
    }
}

export default MenuBar;