import React from 'react';
import $ from 'jquery';

class SalesPage extends React.Component{
    constructor(props){
        super(props);
        this.game = props.game;
        this.state={
            sales:this.game.current_round.products_sales,
            price:this.game.current_round.product_price,
            total:this.calcTotal(this.game.current_round.products_price,this.game.current_round.products_sales),
            stock:this.game.current_company.stock
        };
    }
    calcTotal(price,qtd){
        let total = price*qtd;
        if (isNaN(total)) {
            total = 0;
        }
        return total;
    }
    changeQuantity(e){
        e.target.value = Math.round(e.target.value);
        if (Number(e.target.value) <= this.state.stock && e.target.value>=0) {
            this.changeState({
                sales:Number(e.target.value)
            });
        }
        else{
            alert('Você não pode vender essa quantidade de produtos');
            e.target.value = this.state.stock;
        }
    }
    changePrice(e){
        if (e.target.value > 0) {
            this.changeState({
                price:e.target.value
            });
        }
        else{
            alert('Você não pode vender por menos de 0 reais');
            e.target.value = 0;
        }
    }
    changeState(newState){
        let state ={
            ...this.state,
            ...newState
        };
        state.total = this.calcTotal(state.price,state.sales);
        this.game.Update_Round(undefined,undefined,undefined,undefined,undefined,Number(state.sales),Number(state.price));
        this.setState(state);
    }
    render(){
        return(
            <div className="container">			
                <div className="feed">
                    <div className="qqcoisa ">
                        <div className="flex">
                            <div className="opc">
                                <p>Total</p>

                                <strong className="tot">R$ {Math.floor(this.state.total)}</strong>
                            
                            </div>
                            <div className="opc">
                                <p>Estoque</p>

                                <strong>{this.state.stock}</strong>

                            </div>					
                            
                            <div className="opc">

                                <p>Vender</p>

                                <strong>
                                    <input onChange={this.changeQuantity.bind(this)} type="number" name="" placeholder="000" defaultValue={this.state.sales}/>
                                </strong>
                                <br/><span>Quantidade de produtos a serem vendidos</span>

                            </div>
                            <div className="opc">
                                <p>Preço</p>
                                    <span class="mo">R$</span>
                                <strong>
                                    <input onChange={this.changePrice.bind(this)} type="number" name="" placeholder="000" defaultValue={this.state.price}/>
                                </strong>
                                <br/><span>Valor de cada produto a ser vendido</span>
                            </div>
                        </div>
                    </div>
                </div>	
            </div>
        );
    }
}

export default SalesPage;