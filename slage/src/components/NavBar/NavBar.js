import React from 'react';
import $ from 'jquery';

class NavBar extends React.Component{
    constructor(props){
        super(props);
        this.game = props.game;
        this.game.AddEventListener('state_change',this.stateChangeCallback.bind(this));
        this.game.AddEventListener('future_company_change',this.companyChangeCallback.bind(this));
        this.state = {
            ingame:this.game.state===2,
            money:this.game.future_company.currency,
            resources:this.game.future_company.resource,
            products:this.game.future_company.stock
        }
    }
    companyChangeCallback(){
        if(this.game.current_company!==undefined){
            this.setState({
                ...this.state,
                money:this.game.future_company.currency,
                resources:this.game.future_company.resource,
                products:this.game.future_company.stock
            });
        }
    }
    stateChangeCallback(){
        if(this.game.current_company!==undefined){
            this.setState({
                ...this.state,
                ingame:this.game.state===2
            });
        }
    }
    render(){
        if(!this.state.ingame || this.game.user.bond=='Teacher'){
            return(
            <nav>
                <div className="left">
                    <img src="static/logo-extend.png" alt=""/>
                    <img src="static/logo-name.png" alt="" className="none"/>
                </div>
                <div className="flex">
                </div>
            </nav>);
        }
        else{
            return(
            <nav>
                <div className="left">
                    <img src="static/logo-extend.png" alt=""/>
                    <img src="static/logo-name.png" alt="" className="none"/>
                </div>
                <div className="flex">
                    <div>Dinheiro: {Math.floor(this.state.money)}</div>
                    <div>Matéria prima total: {this.state.resources}</div>
                    <div>Produtos: {this.state.products}</div>
                </div>
            </nav>);
        }
    }
}

export default NavBar;