import React from "react";
import $ from 'jquery';


class LoginForm extends React.Component{
    constructor(props){
        super(props);
        this.game = props.game;
    }
    toSingin(e){
        e.preventDefault();
        this.props.changeForm("singin");
        return false;
    }
    handleSubmit(e){
        e.preventDefault();
        let inputs = $(e.target).serializeArray();
        let fields = [];
        for (let i = 0; i < inputs.length; i++) {
            const e = inputs[i];
            fields[e.name]=e.value;
        }
        
        if(fields['user']!==undefined && fields['email']!==undefined && fields['password']!==undefined && fields['bond']!==undefined){
            this.game.Register(fields['user'],fields['email'],fields['password'],fields['bond']);
        }
        return false;
    }
    render(){
        return <div>
                    <form className="login" onSubmit={this.handleSubmit.bind(this)}>
                        <div className="input-field col s12">
                            <input name="email" id="email" type="email" className="validate" autoFocus/>
                            <label htmlFor="email">Email</label>
                        </div>
                        <div className="input-field col s12">
                            <input name="password" id="password" type="password" className="validate"/>
                            <label htmlFor="password">Senha</label>
                        </div>

                        <div className="input-field col s12 regist">
                            <input name="user" id="text" type="text"/>
                            <label htmlFor="text">Nome</label>
                        </div>

                        <div className="type">
                            <div className="input-field col s6 regist">
                            <select name="bond">
                                <option value="Student">Aluno</option>
                                <option value="Teacher">Professor</option>
                            </select>
                        </div>

                        <div className="input-field regist">
                            <input id="matricula" type="number" className="validate w" maxLength="14"/>
                            <label htmlFor="matricula" className="w">Matrícula</label>
                        </div>

                        <input className="regist mat" type="date"/>
                            <div className="date none regist">
                                <span>Data de Nascimento</span>
                            </div>								
                        </div>

                        <div className="regist">
                            <p>
                                <label>
                                    <input type="checkbox" />
                                    <span>Concordo com os termos</span>
                                </label>
                            </p>
                            <p>	
                                <label>
                                    <input type="checkbox" />
                                    <span>Receber Emails</span>
                                </label>
                            </p>

                        </div>

                        <div className="button">
                            <a onClick={this.toSingin.bind(this)} href="#" className="regist" id="log">Voltar</a>	
                        </div>
                        <button type="submit">
                            <b className="regist">Registrar</b>
                        </button>
                    </form>
                </div>;
    }
}
export default LoginForm;