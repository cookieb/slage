import * as React from "react";
import AuthPage from "../AuthPage/AuthPage";
import GamePage from "../GamePage/GamePage";
import Game from '../../main';

class SiteContainer extends React.Component{
    constructor(props){
        super(props);
        this.game = new Game();
        window.x=this.game;
        this.state = {
            logged:false
        };
        this.game.AddEventListener('state_change',this.stateChangeCallback.bind(this));
    }
    stateChangeCallback(){
        console.log('stateChangeCallback');
        this.setState({
            ...this.state,
            logged:this.game.state!==0
        });
    }
    render(){
        if(this.state.logged===true){
            return (<GamePage game={this.game}></GamePage>);
        }
        else{
            return (<AuthPage game={this.game}></AuthPage>);
        }
    }
}

export default SiteContainer;