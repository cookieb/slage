import React from 'react';
import $ from 'jquery';
import LobbyPage from '../LobbyPage';
import LoungePage from '../LoungePage/LoungePage';
import ServicesPage from '../ServicesPage/ServicesPage';
import SalesPage from '../SalesPage/SalesPage';
import ProductionPage from '../ProductionPage/ProductionPage';
import InvestimentsPage from '../InvestimentsPage/InvestimentsPage';
import AdminLobbyPage from '../AdminLobbyPage/AdminLobbyPage';
import AdminMatchManagerPage from '../AdminMatchManagerPage/AdminMatchManagerPage';

class GameContentContainer extends React.Component{
    constructor(props){
        super(props);
        this.game = props.game;
        this.state = {
            current_page:this.game.current_page,
            game_state:this.game.state
        }
        this.game.AddEventListener('change_page',this.changePageCallback.bind(this));
    }
    changePageCallback(){
        console.log(this.game.current_page);
        this.setState({
            ...this.state,
            game_state:this.game.state,
            current_page:this.game.current_page
        });
    }
    render(){
        if(this.game.user.bond==='Teacher'){
            if(this.state.current_page === 'admin_lounge'){
                return <AdminLobbyPage game={this.game}/>;
            }
            else if(this.state.current_page === 'admin_match'){
                return <AdminMatchManagerPage game={this.game}/>;
            }
            else{
                this.game.ChangePage('admin_lounge');
            }
        }
        else{
            if(this.state.game_state!==2){
                return <LobbyPage game={this.game}/>;
            }
            else{
                if(this.state.current_page === 'lounge'){
                    return <LoungePage game={this.game}/>;
                }
                else if(this.state.current_page === 'services'){
                    return <ServicesPage game={this.game}/>;
                }
                else if(this.state.current_page === 'sales'){
                    return <SalesPage game={this.game}/>;
                }
                else if(this.state.current_page === 'production'){
                    return <ProductionPage game={this.game}/>;
                }
                else if(this.state.current_page === 'investiments'){
                    return <InvestimentsPage game={this.game}/>
                }
                else{
                    this.game.ChangePage('lounge');
                }
            }
        }
        return <span></span>;
    }
}

export default GameContentContainer;