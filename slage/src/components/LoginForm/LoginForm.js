import React from "react";
import $ from 'jquery';

class LoginForm extends React.Component{
    constructor(props){
        super(props);
        this.game = props.game;
    }
    handleLogin(e){
        e.preventDefault();
        let inputs = $(e.target).serializeArray();
        let fields = [];
        for (let i = 0; i < inputs.length; i++) {
            const e = inputs[i];
            fields[e.name]=e.value;
        }
        if(fields['email']!==undefined && fields['password']!==undefined){
            this.game.Login(fields['email'],fields['password']);
        }
        return false;
    }
    toSingup(e){
        e.preventDefault();
        this.props.changeForm("singup");
        return false;
    }
    toRecover(e){
        e.preventDefault();
        this.props.changeForm("recover");
        return false;
    }
    render(){
        return <div>
                    <form onSubmit={this.handleLogin.bind(this)} className="login">
                        <div className="input-field col s12">
                            <input id="email" type="email" name="email" className="validate" defaultValue="" autoFocus/>
                            <label htmlFor="email">Email</label>
                        </div>
                        <div className="input-field col s12">
                            <input id="password" type="password" name="password" className="validate" defaultValue=""/>
                            <label htmlFor="password">Senha</label>
                        </div>
                        <div className="button">
                            <div>
                            <a onClick={this.toSingup.bind(this)} href="#" id="regist">Registrar</a>
                            </div>
                            <div>
                                <a onClick={this.toRecover.bind(this)} href="#" id="forget">Esqueceu a senha?</a>
                            </div>
                        </div>
                        <button type="submit">
                            <b className="entrar">Entrar</b>
                        </button>
                    </form>
                </div>;
    }
}
export default LoginForm;