import React from 'react';

class AdminMatchManagerPage extends React.Component{
    constructor(props){
        super(props);
        this.game = props.game;
        this.state = {
            companies:this.game.company_list,
            rounds:this.game.previous_rounds,
            selected_id:undefined
        }
        this.game.AddEventListener('company_list_change',this.handleCompanyChange.bind(this));
        this.game.AddEventListener('round_list_change',this.handleRoundListChange.bind(this));
    }
    handleCompanyChange(){
        this.setState({
            ...this.state,
            companies:this.game.company_list
        });
    }
    handleRoundListChange(){
        this.setState({
            ...this.state,
            rounds:this.game.previous_rounds
        });
    }
    requestHistory(e){
        this.game.selected_company=e.target.getAttribute('data-id');
        this.game.GetRounds();
    }
    handleCloseRound(){
        this.game.CloseRound();
    }
    render(){
        let companies=[];
        let rounds=[];
        if(this.state.companies!==undefined){
            let context = this;
            this.state.companies.forEach(function(e,i){
                companies.push(<tr key={i}>
                    <td>{e.company_name}</td>
                    <td>{e.marketing_level}</td>
                    <td>{e.infrastructure_level}</td>
                    <td>{e.rh_level}</td>
                    <td>{e.currency}</td>
                    <td>{e.stock}</td>
                    <td>{e.resource}</td>
                    <td><button onClick={context.requestHistory.bind(context)} data-id={e.user_id}>Historico</button></td>
                </tr>);
            });
            this.state.rounds.forEach(function(e,i){
                rounds.push(
                    <tr key={i}>
                        <td>{e.round_num}</td>
                        <td>{e.marketing_evolution==1?'Evoluiu':'-'}</td>
                        <td>{e.infrastructure_evolution==1?'Evoluiu':'-'}</td>
                        <td>{e.rh_evolution==1?'Evoluiu':'-'}</td>
                        <td>{e.purchased_resources}</td>
                        <td>{e.products_made}</td>
                        <td>{e.products_sales}</td>
                        <td>{e.product_price}</td>
                    </tr>
                );
            });
        }
        return (
            <div className="container">	
                <button onClick={this.handleCloseRound.bind(this)}>Próxima rodada</button>
                <div className="feed resp">
                    <div className="qqcoisa">
                        <table>
                            <tbody>
                                <tr>
                                    <th>Nome</th>
                                    <th>Marketing</th>
                                    <th>Infraestrutura</th>
                                    <th>R.H.</th>
                                    <th>Dinheiro</th>
                                    <th>Estoque</th>
                                    <th>Matéria-prima</th>
                                    <th></th>
                                </tr>
                                {companies}
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="feed resp">
                    <div className="qqcoisa">
                        <table>
                            <tbody>
                                <tr>
                                    <th>Rodada</th>
                                    <th>Marketing</th>
                                    <th>Infraestrutura</th>
                                    <th>R.H.</th>
                                    <th>Compra de recursos</th>
                                    <th>Fabricação de recursos</th>
                                    <th>Quantidade vendida</th>
                                    <th>Preço</th>
                                </tr>
                                {rounds}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default AdminMatchManagerPage;