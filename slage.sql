-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 07-Dez-2018 às 08:31
-- Versão do servidor: 10.1.35-MariaDB
-- versão do PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `slage`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `slage_matches`
--

CREATE TABLE `slage_matches` (
  `id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `round_open` tinyint(1) NOT NULL DEFAULT '0',
  `current_round` int(11) NOT NULL DEFAULT '1',
  `code` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `slage_matches`
--

INSERT INTO `slage_matches` (`id`, `owner_id`, `name`, `round_open`, `current_round`, `code`) VALUES
(1, 2, 'Partida do Alberto', 0, 1, 'adaswqe');

-- --------------------------------------------------------

--
-- Estrutura da tabela `slage_match_user`
--

CREATE TABLE `slage_match_user` (
  `user_id` int(11) NOT NULL,
  `match_id` int(11) NOT NULL,
  `company_name` varchar(32) NOT NULL,
  `currency` int(11) NOT NULL DEFAULT '10000',
  `marketing_level` int(11) NOT NULL DEFAULT '0',
  `infrastructure_level` int(11) NOT NULL DEFAULT '0',
  `rh_level` int(11) NOT NULL DEFAULT '0',
  `stock` int(11) NOT NULL DEFAULT '0',
  `resource` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `slage_match_user`
--

INSERT INTO `slage_match_user` (`user_id`, `match_id`, `company_name`, `currency`, `marketing_level`, `infrastructure_level`, `rh_level`, `stock`, `resource`) VALUES
(2, 1, 'company01', 1000, 0, 0, 0, 0, 100),
(3, 1, 'company02', 1000, 0, 0, 0, 0, 100);

-- --------------------------------------------------------

--
-- Estrutura da tabela `slage_rounds`
--

CREATE TABLE `slage_rounds` (
  `match_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `round_num` int(11) NOT NULL,
  `marketing_evolution` tinyint(1) NOT NULL,
  `infrastructure_evolution` tinyint(1) NOT NULL,
  `rh_evolution` tinyint(1) NOT NULL,
  `purchased_resources` int(11) NOT NULL,
  `products_made` int(11) NOT NULL,
  `products_sales` int(11) NOT NULL,
  `product_price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `slage_users`
--

CREATE TABLE `slage_users` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `email` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `bond` set('Student','Teacher','','') NOT NULL DEFAULT 'Student'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `slage_users`
--

INSERT INTO `slage_users` (`id`, `name`, `email`, `password`, `bond`) VALUES
(2, 'alcindo', 'aluno@test', '$2y$10$J/jHZ0AP12MvCvH062NND.uDR.zOavPH06zd5QrvlwmkFRVU1ypjO', 'Student'),
(3, 'aliana', 'aluna@test', '$2y$10$B/yZjsXEgp0RuTdrg35JbOQr2HmTayJ7IoFSsofXqzo2m7UewNWVG', 'Student'),
(4, 'dalila', 'outraaluna@test', '$2y$10$dHqh5YFMXwGP.3tsdyKcuu2IBt5Cymn7nqGsprKTBxjnkgPYtMiRm', 'Student'),
(5, 'alberto', 'professor@test', '$2y$10$9r2yKXdxQ4wGp6JbCynm1OHJcuW.O4QVkGRYDoQX3DlLXlMboe/7e', 'Teacher'),
(6, 'adriano', 'outroaluno@test', '$2y$10$obnpXJZuV6hezsJjA19zluFwCxoTt4HsseSkkpM.f.XxUCnum3K1y', 'Student');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `slage_matches`
--
ALTER TABLE `slage_matches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `owner_id` (`owner_id`);

--
-- Indexes for table `slage_match_user`
--
ALTER TABLE `slage_match_user`
  ADD PRIMARY KEY (`user_id`,`match_id`),
  ADD KEY `match_user_ibfk_2` (`match_id`);

--
-- Indexes for table `slage_rounds`
--
ALTER TABLE `slage_rounds`
  ADD PRIMARY KEY (`match_id`,`user_id`,`round_num`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `slage_users`
--
ALTER TABLE `slage_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `slage_matches`
--
ALTER TABLE `slage_matches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `slage_users`
--
ALTER TABLE `slage_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `slage_matches`
--
ALTER TABLE `slage_matches`
  ADD CONSTRAINT `slage_matches_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `slage_users` (`id`);

--
-- Limitadores para a tabela `slage_match_user`
--
ALTER TABLE `slage_match_user`
  ADD CONSTRAINT `slage_match_user_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `slage_users` (`id`),
  ADD CONSTRAINT `slage_match_user_ibfk_2` FOREIGN KEY (`match_id`) REFERENCES `slage_matches` (`id`);

--
-- Limitadores para a tabela `slage_rounds`
--
ALTER TABLE `slage_rounds`
  ADD CONSTRAINT `slage_rounds_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `slage_users` (`id`),
  ADD CONSTRAINT `slage_rounds_ibfk_2` FOREIGN KEY (`match_id`) REFERENCES `slage_matches` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
