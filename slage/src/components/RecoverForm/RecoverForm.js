import React from "react";

class RecoverForm extends React.Component{
    toLogin(e){
        e.preventDefault();
        this.props.changeForm("singin");
        return false;
    }
    render(){
        return <form action="POST" className="forget">				
                    <div className="input-field col s12">
                        <input id="email" type="email" className="validate"/>
                        <label htmlFor="email">Email</label>
                    </div>
                    <div className="button">
                        <a onClick={this.toLogin.bind(this)} href="#" className="" id="back">Voltar</a>	
                    </div>
                    <button type="submit"><b>Enviar</b></button>                
                </form>;
    }
}

export default RecoverForm;