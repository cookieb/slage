import React from 'react';
import $ from 'jquery';

class ProductionPage extends React.Component{
    constructor(props){
        super(props);
        this.game = props.game;
        this.state = {
            make:this.game.current_round.products_made,
            resources_used:this.game.UsedResources(this.game.current_round.products_made),
            cost:this.game.MakeCost(this.game.current_round.products_made)
        };
    }
    usedResources(total_to_make){
        return 0;
    }
    makeCost(total_to_make){
        return 0;
    }
    changeQuantity(e){
        e.target.value = Math.round(e.target.value);
        if (this.game.UsedResources(Number(e.target.value)) <= this.game.current_company.resource && e.target.value>=0) {
            this.changeState({
                make:Number(e.target.value),
            });
        }
        else{
            alert('Você não produzir essa quantidade.');
            e.target.value = this.state.make;
        }
    }
    changeState(newState){
        let state ={
            ...this.state,
            ...newState,
            resources_used:this.game.UsedResources(newState.make),
            cost:this.game.MakeCost(newState.make)
        };
        this.game.Update_Round(undefined,undefined,undefined,undefined,Number(state.make),undefined,undefined);
        this.setState(state);
    }
    render(){
        return(
            <div className="container">	
                <div className="feed resp">
                    <div className="qqcoisa">
                        <div>
                            <h3>Produzir mercadoria</h3>
                        </div>
                        <div className="produto">
                            <div className="flex">
                                <div className="opc">
                                    <p>Produzir</p>
                                    <strong>
                                        <input onChange={this.changeQuantity.bind(this)} type="number" name="" placeholder="000" defaultValue={this.state.make}/>
                                    </strong>
                                    <br/><span>Quantidade de produtos a serem produzidos</span>
                                </div>
                                <div className="opc">
                                    <p>Matéria prima</p>
                                    <strong>{this.state.resources_used}</strong>
                                </div>
                                <div className="opc">
                                    <p>Custo de fabricação</p>
                                    <strong>R$ {this.state.cost}</strong>
                                </div>					
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ProductionPage;