import $ from 'jquery';

class Game{
    loginListeners = [];
    logoutListeners = [];
    matchListUpdatedListener = [];
    matchJoinListener = [];
    companyUpdatedListener = [];
    matchUpdatedListener = [];
    changePageListener = [];
    adminInfoUpdatedListener = [];
    matchList = undefined;
    matchListLastUpdate = 0;
    current_page=undefined;
    token=undefined;
    header = undefined;
    content = undefined;
    current_round=undefined;
    rounds = undefined;
    match = undefined;
    match_id=undefined;
    company = undefined;
    company_round = undefined;
    resource_cost=0;
    sale_stock_price =0;
    update_wait = 0;
    managing = false;
    match_id = undefined;
    admin_match = undefined;
    admin_companies = undefined;
    state = 'login'; //login, logged, ingame
    baseUrl='http://189.124.190.4/';

    constructor(){
        this.interval = setInterval(this.fixedUpdate.bind(this),250);
        window.game=this;
    }
    sendRound(){

    }
    getAdminInfo(match_id){
        if(this.content.bond ==='Teacher' && match_id!==undefined){
            this.requestJSON('/match/'+match_id,'GET',function(jqXHR){
                if(jqXHR.status===200){
                    if(jqXHR.responseJSON.match!==undefined, jqXHR.responseJSON.companies!==undefined){
                        this.admin_companies = jqXHR.responseJSON.companies; 
                        this.match_id = jqXHR.responseJSON.match.id;
                        this.admin_match = jqXHR.responseJSON.match;
                        this.requestJSON('/match/'+this.match_id+'/round/all','GET',function(jqXHR){
                            if(jqXHR.status===200){
                                if(jqXHR.responseJSON.rounds!==undefined){
                                    let rounds = jqXHR.responseJSON.rounds;
                                    while(rounds.length!==0){
                                        let round = rounds.pop();
                                        for(let i =0; i<this.admin_companies.length;i++){
                                            if(this.admin_companies[i].user_id==round.user_id){
                                                if(this.admin_companies[i].rounds===undefined){
                                                    this.admin_companies[i].rounds=[];
                                                }
                                                this.admin_companies[i].rounds.push(round);
                                            }
                                        }
                                    }
                                    for(let i =0; i<this.admin_companies.length;i++){
                                        if(this.admin_companies[i].rounds===undefined){
                                            this.admin_companies[i].rounds=[];
                                        }
                                        this.admin_companies[i].rounds.sort(function(a,b){return a.round_num<b.round_num});
                                    }
                                    this.adminInfoUpdatedListener.forEach(element => {
                                        if(typeof(element)==='function'){
                                            try{
                                                element();
                                            }
                                            catch(ex){
                                                console.log(ex);
                                            }
                                        }
                                    });
                                }
                            }
                        }.bind(this),undefined,true);
                    }
                }
            }.bind(this),undefined,true);
        }
    }
    changeCompany(changes){
        this.company = {...this.company, ...changes};
        this.companyUpdatedListener.forEach(element => {
            if(typeof(element)==='function'){
                try{
                    element();
                }
                catch(ex){
                    console.log(ex);
                }
            }
        });
    }
    tokenRefresh(){

    }
    loggedUpdate(){
        console.log('logged update');
        this.requestJSON('match/all/changed/'+this.matchListLastUpdate, 'GET', function(jqXHR){
            if(jqXHR.status === 200 && jqXHR.responseJSON.diff !== undefined && jqXHR.responseJSON.diff === true){
                this.requestJSON('match/all', 'GET', function(jqXHR){
                    console.log(jqXHR.responseJSON);
                    if(jqXHR.responseJSON.matchs!==undefined && jqXHR.responseJSON.changed!==undefined){
                        this.matchList = jqXHR.responseJSON.matchs;
                        this.matchListLastUpdate = jqXHR.responseJSON.changed;
                        this.matchListUpdatedListener.forEach(element => {
                            if(typeof(element)==='function'){
                                try{
                                    element();
                                }
                                catch(ex){
                                    console.log(ex);
                                }
                            }
                        });
                    }
                }.bind(this),undefined,true);
            }
        }.bind(this));
    }
    ingameUpdate(){
        console.log('ingame update');

    }
    setLogin(token){
        if(this.state==='login'){
            console.log('Setting token: ',token);
            let parts = token.split('.');
            if(parts.length === 3){
                let header = JSON.parse(atob(parts[0]));
                let content = JSON.parse(atob(parts[1]));
                this.header = header;
                this.content = content;
                this.token = token;
                this.state = 'logged';
                this.loginListeners.forEach(element => {
                    if(typeof(element)==='function'){
                        try{
                            element();
                        }
                        catch(ex){
                            console.log(ex);
                        }
                    }
                });
            }
        }
    }
    roundSale(qtd,price){
        if(this.state==='ingame'){
            this.current_round.products_sales=qtd;
            this.current_round.product_price=price;
            this.changeCompany({stock:this.company.stock-qtd});
        }
        return false;
    }
    roundService(material,cost){
        if(this.state==='ingame'){
            cost = Number(cost);
            if(this.current_round.purchased_resources!==0){
                this.company.currency=Number(this.company.currency)+this.resource_cost;
            }
            this.current_round.purchased_resources = material;
            this.company.currency=Number(this.company.currency)-cost;
            this.resource_cost=cost;
            this.sendRound();
            this.companyUpdatedListener.forEach(element => {
                if(typeof(element)==='function'){
                    try{
                        element();
                    }
                    catch(ex){
                        console.log(ex);
                    }
                }
            });
            return true;
        }
        return false;
    }
    getServices(){
        let services=[];
        let total = 3+Math.floor(Math.sin(this.match.current_round*6)*2);
        for(let i=0;i<total;i++){
            let material=(i+1)*Math.floor(200-Math.sin(this.match.current_round)*100);
            let cost=10+Math.sin(this.match.current_round+i)-this.company_round.infrastructure_level/2;
            services.push({
                material:material,
                cost:cost*material
            });
        }
        return services;
    }
    joinMatch(match_id){
        if(this.state==='logged'){
            this.match_id = match_id;
            this.requestJSON('/match/'+match_id+'/join','POST',function(jqXHR){
                console.log(jqXHR.responseJSON);
                if(jqXHR.status===201){
                    this.requestJSON('/match/'+this.match_id,'GET',function(jqXHR){
                        if(jqXHR.status===200){
                            if(jqXHR.responseJSON.match!==undefined && jqXHR.responseJSON.company !== undefined){
                                this.match = jqXHR.responseJSON.match;
                                this.company = jqXHR.responseJSON.company;
                                this.company_round = this.company;
                                this.rounds = [];
                                this.current_round = {round_num: 0,marketing_evolution: 0,infrastructure_evolution: 0,rh_evolution: 0,purchased_resources: 0,products_made: 0,products_sales: 0,product_price: 0};
                                this.changeCompany({});
                            }
                            this.state='ingame';
                            this.matchJoinListener.forEach(element => {
                                if(typeof(element)==='function'){
                                    try{
                                        element();
                                    }
                                    catch(ex){
                                        console.log(ex);
                                    }
                                }
                            });
                            this.changePage('lounge');
                            this.requestJSON('/match/'+this.match_id+'/round/all','GET',function(jqXHR){
                                if(jqXHR.status===200){
                                    if(jqXHR.responseJSON.rounds!==undefined){
                                        this.rounds = jqXHR.responseJSON.rounds;
                                        this.rounds.sort(function(a,b){return a.round_num>b.round_num});
                                        if(this.rounds[0].round_num === this.match.current_round){
                                            this.current_round = this.rounds[0];
                                        }
                                    }
                                }
                                else if(jqXHR.status===204){
                                    console.log('Nenhum round foi encontrado');
                                }
                                else if(jqXHR.status===404){
                                    alert('A compania não existe.');                                    
                                }
                                else if(jqXHR.status===400){
                                    console.log('JoinMatch bad request');
                                }
                                else if(jqXHR.status===401){
                                    alert('Parece que você não está logado, por favor efetue o login e tente novamente.');
                                    this.logout();                                    
                                }
                                else{
                                    console.log('Get /match/{id}/round/all erro fatal ',jqXHR.status);
                                }
                            }.bind(this),undefined,true);
                        }
                        else if(jqXHR.status===404){
                            alert('A partida não existe mais.');                            
                        }
                        else if(jqXHR.status===400){
                            console.log('JoinMatch bad request');
                        }
                        else if(jqXHR.status===401){
                            alert('Parece que você não está logado, por favor efetue o login e tente novamente.');
                            this.logout();
                        }
                        else{
                            console.log('Get /match/{id} erro fatal ',jqXHR.status);
                        }
                    }.bind(this),undefined,true);
                }else if(jqXHR.status===401){
                    alert('Parece que você não está logado, por favor efetue o login e tente novamente.');
                    this.logout();
                } else if(jqXHR.status===404){
                    alert('Parece que a partida na qual você tentou entrar não existe.');
                } else if(jqXHR.status===400){
                    console.log('JoinMatch bad request');
                } else{
                    console.log('JoinMatch erro fatal ',jqXHR.status);
                }
            }.bind(this),undefined,true);
        }
    }
    logout(){
        this.token = undefined;
        this.header = undefined;
        this.content = undefined;
        this.state = "login";
        this.logoutListeners.forEach(element => {
            if(typeof(element)==='function'){
                try{
                    element();
                }
                catch(ex){
                    console.log(ex);
                }
            }
        });
    }
    changePage(page){
        if(this.state!=='login' && 
            ((this.content.bond!=='Teacher' && (page==='lounge'||page==='services'||page==='sales'||page==='production'||page==='investiments')))||
            (this.content.bond==='Teacher' && (page==='admin_lounge'||page==='admin_match'))){
            this.current_page = page;
            this.changePageListener.forEach(element => {
                if(typeof(element)==='function'){
                    try{
                        element();
                    }
                    catch(ex){
                        console.log(ex);
                    }
                }
            });
        }
    }
    requestJSON(path,method,callback,data,auth){
        if( path === undefined ||
                (method !==undefined && method!=='GET' && method!=='POST')
            ){

            return false;
        }
        if(auth === undefined){
            auth = false;
        }
        if(callback === undefined){
            callback = this.noOP;
        }
        if(method === undefined){
            method = 'GET';
        }
        if(data === undefined||typeof(data)!=='object'){
            data = {};
        }
        console.log(path,method,callback,data,auth);
        let requestUrl = this.baseUrl + path;
        let token = this.token;
        $.ajax({
            url:requestUrl,
            method:method,
            data:data,
            complete:callback,
            beforeSend:function(jqXHR){
                if(auth){
                    jqXHR.setRequestHeader('Authorization','JWT '+token);
                }
            }
        });
    }
    addEventListener(event,callback){
        if(callback===undefined){
            return false;
        }
        switch(event){
            case "onLogin":
                this.loginListeners.push(callback);
                break;
            case "onLogout":
                this.logoutListeners.push(callback);
                break;
            case "onJoin":
                this.matchJoinListener.push(callback);
                break;
            case "onMatchUpdate":
                this.matchUpdatedListener.push(callback);
                break;
            case "onMatchListUpdate":
                this.matchListUpdatedListener.push(callback);
                break;
            case "onChangePage":
                this.changePageListener.push(callback);
                break;
            case "onCompanyUpdate":
                this.companyUpdatedListener.push(callback);
                break;
            case "onAdminInfoUpdated":
                this.adminInfoUpdatedListener.push(callback);
                break;
            default:
                return false;
        }
        return true;
    }
    fixedUpdate(){
        this.update_wait+=1;
        if(this.state!=='login'){
            if(this.state==='logged'){
                if(this.update_wait>=8){
                    this.update_wait = 0;
                    this.tokenRefresh();
                    this.loggedUpdate();
                }
            }
            else if(this.state==='ingame'){
                if(this.update_wait>=4){
                    this.update_wait = 0;
                    this.tokenRefresh();
                    this.ingameUpdate();
                }
            }
        }
    }
    noOP(a,b){

    }
}

export default Game;