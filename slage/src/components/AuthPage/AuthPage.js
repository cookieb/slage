import React from "react";
import LoginForm from "../LoginForm/LoginForm";
import "../../resources/css/materialize.css";
import "../../resources/css/style_login.css";
import RegisterForm from "../RegisterForm";
import RecoverForm from "../RecoverForm";

class AuthPage extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            current:"singin"
            //current:"singup"
            //current:"recover"
        };
        this.game = props.game;
    }
    changeForm(value){
        if(value==="singin"||value==="singup"||value==="recover"){
            this.setState({
                ...this.state,
                current:value
            });
        }
    }
    render(){

        const MyCurrentForm = (props)=>{
            if(this.state.current === "singup"){
                return <RegisterForm game={this.game} changeForm={this.changeForm.bind(this)}/>;
            }
            else if(this.state.current === "recover"){
                return <RecoverForm game={this.game} changeForm={this.changeForm.bind(this)}/>;
            }
            else{
                return <LoginForm game={this.game} changeForm={this.changeForm.bind(this)}/>;
            }
        };

        return (
        <div>
            <div className="log">
                {MyCurrentForm()}
            </div>
        </div>);
    }
}

export default AuthPage;