import React from 'react';
import $ from 'jquery';

class ServicesPage extends React.Component{
    constructor(props){
        super(props);
        this.game=props.game;
        this.state={
            purchased:this.game.current_round.purchased_resources
        };
    }
    selectService(e){
        let choice = e.target.getAttribute('data-choice');
        let cost = e.target.getAttribute('data-price');
        if(this.game.Update_Round(undefined,undefined,undefined,Number(cost),undefined,undefined)){
            this.setState({
                ...this.state,
                purchased:choice
            });
        }
    }
    render(){
        let services = [];
        let services_data = this.game.GetServices();
        for(let i=0;i<services_data.length;i++){
            services.push(
                <label key={i} className="trans">
                    <div>
                        <ul>
                            <li>Nome: Salgados LTDA</li>
                            <li>Oferta: {services_data[i].material} recursos</li>
                            <li>Custo: {Math.ceil(services_data[i].cost)} duavis</li>
                            <li>
                                <button onClick={this.selectService.bind(this)} data-price={services_data[i].cost} data-choice={services_data[i].material}>
                                    {this.state.purchased==services_data[i].cost?'Selecionado':'Selecionar'}
                                </button>
                            </li>
                        </ul>
                    </div>
                </label>
            );
            console.log(i);
        }
        return(
            <div className="container">
                <div className="feed resp qqcoisa">
                    <h4>Comprar Matéria-prima</h4>
                    <h5>Escolha um dos fornecedores abaixo:</h5>
                    <div className="flex">
                        {services}
                    </div>
                </div>
            </div>
        );
    }
}

export default ServicesPage;