import React from 'react';
import $ from 'jquery';

class InvestimentsPage extends React.Component{
    constructor(props){
        super(props);
        this.game=props.game;
        this.state={
            marketing_level:this.game.future_company.marketing_level,
            infraestructure_level:this.game.future_company.infrastructure_level,
            rh_level:this.game.future_company.rh_level
        };
        this.game.AddEventListener('future_company_change',this.handleFutureCompanyChange.bind(this));
    }
    upgradeM(){
        this.game.Update_Round(1,undefined,undefined,undefined,undefined,undefined,undefined,undefined);
    }
    upgradeI(){
        this.game.Update_Round(undefined,1,undefined,undefined,undefined,undefined,undefined,undefined);
    }
    upgradeR(){
        this.game.Update_Round(undefined,undefined,1,undefined,undefined,undefined,undefined,undefined);
    }
    handleFutureCompanyChange(){
        this.setState({
            ...this.state,
            marketing_level:this.game.future_company.marketing_level,
            infraestructure_level:this.game.future_company.infrastructure_level,
            rh_level:this.game.future_company.rh_level
        });
    }
    render(){
        return(
            <div className="feed resp">
                <div classNameNam="qqcoisa">
                    <div className="flex-1">
                        <div className="ope div">
                            <div>
                                <h2>Marketing</h2>
                            </div>
                            <div>
                                <p className="inli">Nível:  {this.state.marketing_level}</p>
                                <button onClick={this.upgradeM.bind(this)} className="submit">Aumentar nível</button>
                            </div>
                            <div>
                                <p className="inli">Custo de evolução:</p>
                                <p className="inli">R$ {(Number(this.game.future_company.marketing_level)+1)*100}</p>
                            </div>
                            <div>
                                <p className="inli">Custo Mensal:</p>
                                <p className="inli">R$ {Number(this.game.future_company.marketing_level)*20}</p>
                                <span>
                                    <i className="fas fa-angle-double-right"></i>
                                </span>
                                <p className="inli">R$ {(Number(this.game.future_company.marketing_level)+1)*20}</p>
                            </div>
                            <div>
                                <p className="inli">Preço do Produto:</p>
                                <p className="inli">R$ {1+(Number(this.game.future_company.marketing_level))*5}</p>
                                <span>
                                    <i className="fas fa-angle-double-right"></i>
                                </span>
                                <p className="inli">R$ {1+(Number(this.game.future_company.marketing_level)+1)*5}</p>
                            </div>						
                        </div>
                        <div className="ope div">
                            <div>
                                <h2>Infraestrutura</h2>
                            </div>
                            <div>
                                <p className="inli">Nível:  {this.state.infraestructure_level}</p>
                                <button onClick={this.upgradeI.bind(this)} className="submit">Aumentar nível</button>
                            </div>
                            <div>
                                <p className="inli">Custo de evolução:</p>
                                <p className="inli">R$ {(Number(this.game.future_company.infrastructure_level)+1)*100}</p>
                            </div>
                            <div>
                                <p className="inli">Custo Mensal:</p>
                                <p className="inli">R$ {(Number(this.game.future_company.infrastructure_level))*20}</p>
                                <span>
                                    <i className="fas fa-angle-double-right"></i>
                                </span>
                                <p className="inli">R$ {(Number(this.game.future_company.infrastructure_level)+1)*100}</p>
                            </div>
                            <div>
                                <p className="inli">Produção Maxima:</p>
                                <p className="inli">+{(Number(this.game.future_company.infrastructure_level)+0)*100}</p>
                                <span>
                                    <i className="fas fa-angle-double-right"></i>
                                </span>
                                <p className="inli">+{(Number(this.game.future_company.infrastructure_level)+1)*100}</p>
                            </div>
                            <div>
                                <p className="inli">MP/Produto:</p>
                                <p className="inli">{(11-Number(this.game.future_company.infrastructure_level))}</p>
                                <span>
                                    <i className="fas fa-angle-double-right"></i>
                                </span>
                                <p className="inli">{(10-Number(this.game.future_company.infrastructure_level))}</p>
                            </div>							
                        </div>
                        <div className="ope div">
                            <div>
                                <h2>RH</h2>
                            </div>
                            <div>
                                <p className="inli">Nível:  {this.state.rh_level}</p>
                                <button onClick={this.upgradeR.bind(this)} className="submit">Aumentar nível</button>
                            </div>
                            <div>
                                <p className="inli">Custo de evolução:</p>
                                <p className="inli">R$ {(Number(this.game.future_company.rh_level)+1)*100}</p>
                            </div>
                            <div>
                                <p className="inli">Custo Mensal:</p>
                                <p className="inli">R$ {(Number(this.game.future_company.rh_level))*20}</p>
                                <span>
                                    <i className="fas fa-angle-double-right"></i>
                                </span>
                                <p className="inli">R$ {(Number(this.game.future_company.rh_level)+1)*20}</p>
                            </div>
                            <div>
                                <p className="inli">Produção Maxima:</p>
                                <p className="inli">+{(Number(this.game.future_company.rh_level))*50}</p>
                                <span>
                                    <i className="fas fa-angle-double-right"></i>
                                </span>
                                <p className="inli">+{(Number(this.game.future_company.rh_level)+1)*50}</p>
                            </div>						
                        </div>
                    </div>
                </div>						
            </div>
        );
    }
}

export default InvestimentsPage;