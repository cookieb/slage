import React from 'react';
import $ from 'jquery';

class LobbyPage extends React.Component{
    constructor(props){
        super(props);
        this.game = props.game;
        this.game.AddEventListener('match_list_change',this.matchListUpdatedCallback.bind(this));
        this.state={
            matchList:this.game.match_list
        };
    }
    matchListUpdatedCallback(){
        this.setState({
            ...this.state,
            matchList:this.game.match_list
        });
    }
    handleMatchJoin(e){
        let code = e.target.getAttribute('data-code');
        this.game.JoinMatch(code);
        e.preventDefault();
        return false;
    }
    render(){
        let matchs=[];
        if(this.state.matchList!==undefined){
            let context=this;
            this.state.matchList.forEach(function(e,i){
                matchs.push(
                    <tr key={i}>
                        <td>{e.name}</td>
                        <td>{e.current_round}</td>
                        <td>{e.code}</td>
                        <td><button onClick={context.handleMatchJoin.bind(context)} data-code={e.id}>Entrar</button></td>
                    </tr>
                );
            });
        }
        return (
        <div className="container">	
            <div className="feed resp">
                <div className="qqcoisa">
                    <table>
                        <tbody>
                            <tr>
                                <th>Nome</th>
                                <th>Rodada atual</th>
                                <th>Código</th>
                                <th></th>
                            </tr>
                            {matchs}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>);
    }
}

export default LobbyPage;