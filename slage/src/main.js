import $ from 'jquery';

class Game{

    /**
     * -- Estados
     *      *Deslogado:     (0) Não tenta puxar nenhuma atualização
     *      *Logado:        (1) Tenta puxar atualização apenas da lista de partidas
     *      *EmPartida:     (2) Tenta puxar as atualizações da partida atual
     *      *Gerenciando:   (3) Tenta puxar as atualizações dos jogadores da partida atual
     * -- Companhia tem
     *      match_id:               O id da partida a qual essa companhia pertence
     *      user_id:                O id do usuário ao qual essa companhia pertence
     *      company_name:           O nome dessa companhia
     *      currency:               O dinheiro dessa companhia
     *      marketing_level:        O nível de marketing dessa companhia
     *      infrastructure_level:   O nível de infraestrutura dessa companhia
     *      rh_level:               O nível de RH dessa companhia
     *      stock:                  O estoque de produtos dessa companhia
     *      resource:               A quantidade de matéria-prima dessa companhia
     * -- Listeners
     *      state_change            Quando o estado do jogo muda
     *      round_change            Quando as informações do round mudam
     *      future_company_change   Quando a companhia futura muda
     *      company_change          Quando a companhia atual muda
     * -- Erros
     *     -1-Nenhum erro               0-Email e senha errados             
     *      1-Email inexistente         2-Forneça email e senha
     *      3-Logado com sucesso        
     */

    constructor(){
        this.last_error = -1;
        this.state = 0;
        this._interval_delay=100;
        this._waiting = false;
        this._update_count = 0;
        this.current_page = 'lobby';
        this._token = undefined;
        
        this.user = this._CreateEmptyUser();
        this._match = this._CreateEmptyMatch();
        this.current_round = this._CreateEmptyRound();
        this.current_company = this._CreateEmptyCompany();
        this.future_company = this._CreateEmptyCompany();

        this.company_list = [];
        this.selected_company = undefined;

        this._listeners = {};
        this._listeners_names = [];
        this.previous_rounds = [];
        this._commands = {};
        this.match_list = [];
        this._api_url = document.body.getAttribute('data-url');

        if(this._api_url == null){
            this._api_url = 'https://fases.ifrn.edu.br/slage/slapi/index.php';
        }

        this._listeners_names = [
            'state_change',
            'round_change',
            'future_company_change',
            'change_page',
            'company_change',
            'company_list_change',
            'round_list_change',
            'match_list_change',
            'match_change',
            'on_error'
        ];
        for (let i = 0; i < this._listeners_names.length; i++) {
            const e = this._listeners_names[i];
            this._listeners[e]=[];
        }

        this._interval = setInterval(this._Update.bind(this),this._interval_delay);

    }
    ChangePage(page){
        this.current_page = page;
        this._FireListeners('change_page');
    }
    GetRounds(){
        this._commands['round/all']={user_id:this.selected_company, match_id:this._match.id};
    }
    CloseRound(){
        this._commands['round/close']={match_id:this._match.id};
    }
    Login(email,password){
        this._commands['auth/token']={
            email:email,
            password:password
        };
    }
    Logout(){
        this.user = this._CreateEmptyUser();
        this._token = undefined;
        this.state=0;
        this._FireListeners('state_change');
    }
    Register(user,email,password,bond){
        this._commands['user/create']={
            user:user,
            email:email,
            password:password,
            bond:bond
        };
    }
    JoinMatch(match_id){
        this._commands['match/join']={match_id:match_id};
    }
    LeaveMatch(){

    }
    Update_Round(marketing_evolution, infrastructure_evolution, rh_evolution, purchased_resources, products_made, products_sales, product_price){
        let changed = false;
        let upgraded = false;
        this._SendRound();
        //marketing_evolution
        if(marketing_evolution!==undefined  && this.current_round.marketing_evolution!=1 ){
            this.current_round.marketing_evolution=marketing_evolution;
            this.future_company.marketing_level++;
            changed = true;
            upgraded= true;
        }
        //infrastructure_evolution
        if(infrastructure_evolution!==undefined && this.current_round.infrastructure_evolution!=1){
            this.current_round.infrastructure_evolution=infrastructure_evolution;
            this.future_company.infrastructure_level++;
            changed = true;
            upgraded= true;
        }
        //rh_evolution
        if(rh_evolution!==undefined  && this.current_round.rh_evolution!=1){
            this.current_round.rh_evolution=rh_evolution;
            this.future_company.rh_level++;
            changed = true;
            upgraded= true;
        }
        //purchased_resources
        if(purchased_resources!==undefined && typeof(purchased_resources) === 'number'){
            this.current_round.purchased_resources=purchased_resources;
            changed = true;
        }
        //products_made
        if(products_made!==undefined && typeof(products_made) === 'number'){
            this.current_round.products_made=products_made;
            changed = true;
        }
        //products_sales
        if(products_sales!==undefined && typeof(products_sales) === 'number'){
            this.current_round.products_sales=products_sales;
            changed = true;
        }
        //product_price
        if(product_price!==undefined && typeof(product_price) === 'number'){
            this.current_round.product_price=product_price;
            changed = true;
        }

        //Alguma coisa mudou?
        if(changed===true){
            this._FireListeners('round_change');
        }
        if(upgraded===true){
            this._FireListeners('future_company_change');
        }
        return changed;
    }
    AddEventListener(listener,callback){
        const index = this._listeners_names.indexOf(listener);
        if(index !== -1){
            const listener = this._listeners[this._listeners_names[index]];
            if(typeof(callback)==='function'){
                listener.push(callback);
                return true;
            }
        }
        return false;
    }
    GetServices(){
        let services_data = [];
        for (let i = 0; i < 3; i++) {
            services_data.push({cost:(i+1)*100,material:(i+1)*100});
        }
        return services_data;
    }
    MakeCost(qtd){
        return qtd;
    }
    ManageMatch(match_id){
        this._commands['company/list'] = {match_id:match_id};
        for (let i = 0; i < this.match_list.length; i++) {
            const e = this.match_list[i];
            if(e.id==match_id){
                this._match = e;
            }
        }
        this._FireListeners('company_list_change');
    }
    UsedResources(qtd){
        return qtd;
    }
    _SendRound(){
        if(this.state===2){
            this._commands['round/send']={round:this.current_round};
        }
    }
    _SendCommands(){
        if(Object.keys(this._commands).length!==0){
            let post_data = {commands:JSON.stringify({commands:this._commands,token:this._token})};
            $.ajax({
                //url:'http:/fasew.ifrn.edu/slage/slapi/index.php',
                //url:'https://fases.ifrn.edu.br/slage/slapi/index.php',
                url:this._api_url,
                method:'POST',
                data:post_data,
                complete:(function(JQxhr){
                    if(JQxhr.responseText !==undefined){
                        let json = JSON.parse(JQxhr.responseText);
                        this._ResponseParser(json);
                    }
                }).bind(this)
            });
            this._commands={};
        }
    }
    _ResponseParser(responseArray){
        const commands = Object.keys(responseArray.commands);
        let events = {};

        for (let i = 0; i < commands.length; i++) {
            const e = responseArray.commands[commands[i]];
            switch(commands[i]){
                case 'auth/token':
                    if(e.status===200 &&  e.token!==undefined){
                        // sucesso total
                        this._token = e.token;
                        let user = JSON.parse(atob(this._token.split('.')[1]));
                        this.user = {
                            ...this.user,
                            ...user
                        };
                        this.state = 1;
                        events['state_change']=true;
                    }
                    else if(e.status===401){
                        alert('A combinação email e senha está incorreta');
                    }
                    else if(e.status === 404){
                        alert('O email não foi encontrado');
                    }
                    break;
                case 'match/list':
                    if(e.status===200 &&  e.matchs!==undefined){
                        // sucesso total
                        this.match_list = e.matchs;
                        events['match_list_change']=true;
                    }
                    break;
                case 'match/join':
                    if(e.status===200 &&  e.match_id!==undefined){
                        // sucesso total
                        for (let j = 0; j < this.match_list.length; j++) {
                            const element = this.match_list[j];
                            if(element.id==e.match_id){
                                this._match = element;
                                this.state = 2;
                                this.current_round.round_num = this._match.current_round;
                                this.current_round.match_id = this._match.id;
                                this.current_round.user_id = this.user.id;
                                this._commands['company/get']={match_id:this._match.id,user_id:this.user.id};
                                this._commands['round/all']={match_id:this._match.id,user_id:this.user.id};
                                this._SendCommands();
                                this.ChangePage('lounge');
                                events['state_change']=true;
                                break;
                            }
                        }
                        events['match_change']=true;
                    }
                    break;
                case 'match/get':
                    if(e.status===200 &&  e.match!==undefined){
                        // sucesso total
                        if(e.match.current_round!=this.current_round.round_num){
                            alert('O round mudou. Round atual: '+e.match.current_round);
                            this._match = e.match;
                            this.previous_rounds.push(this.current_round);
                            this.current_round = this._CreateEmptyRound();
                            this.current_round.match_id = this._match.id;
                            this.current_round.round_num = this._match.current_round;
                            this.current_round.user_id = this.user.id;
                            this._commands['company/get']={match_id:this._match.id,user_id:this.user.id};
                            this.ChangePage('lobby');
                            this._SendCommands();
                        }
                        events['match_change']=true;
                        events['round_list_change']=true;
                    }
                    break;
                case 'company/get':
                    if(e.status===200 &&  e.company!==undefined){
                        // sucesso total
                        this.current_company = e.company;
                        this.future_company = this.current_company;
                        events['company_change']=true;
                        events['future_company_change']=true;
                    }
                    break;
                case 'company/list':
                    if(e.status===200 &&  e.companies!==undefined){
                        // sucesso total
                        this.company_list = e.companies;
                        events['company_list_change']=true;
                    }
                    break;
                case 'round/all':
                    if(e.status===200 &&  e.rounds!==undefined){
                        // sucesso total
                        this.previous_rounds = e.rounds;
                        events['round_list_change']=true;
                    }
                    break;
                case 'user/create':
                    if(e.status===200){
                        // sucesso total
                        alert('Usuário criado com sucesso');
                    }
                    break;
                default:
                    break;
            }
        }

        events = Object.keys(events);

        for (let i = 0; i < events.length; i++) {
            const e = events[i];
            this._FireListeners(e);
        }
    }
    _Update(){
        this._update_count++;
        switch(this.state){
            case 0:
                if(this._update_count%3==0 && (this._commands['auth/token']!==undefined || this._commands['user/create']!==undefined)){
                    this._SendCommands();
                }
                break;
            //Procurar novas matchs
            case 1:
                if(this._update_count%5==0){
                    if(this._commands['match/join']!==undefined){
                        this._SendCommands();
                    }
                    else if(this._update_count%50==0){
                        this._commands['match/list']={changed:0};
                        this._SendCommands();
                    }
                }
                break;
            //Verificar se a partida mudou
            case 2:
                if(this._update_count%5==0){
                    if(this._update_count%20==0){
                        this._commands['match/get'] = {match_id:this._match.id};
                    }
                    this._SendCommands();
                }
                break;
            default:
                break;
        }
    }
    _SetError(code){
        if(typeof(code)==='number'){
            this.last_error = code;
            this._FireListeners('on_error');
        }
    }
    _FireListeners(listener){
        const index = this._listeners_names.indexOf(listener);
        if(index !== -1){
            const listener = this._listeners[this._listeners_names[index]];
            for (let i = 0; i < listener.length; i++) {
                const e = listener[i];
                try{
                   e();
                }
                catch(ex){
                    console.log(ex);
                }
            }
        }
    }
    _CreateEmptyMatch(){
        return {
            id:undefined,
            owner_id:undefined,
            name:'',
            open:0,
            round_open:0,
            current_round:0,
            max_rounds:0
        };
    }
    _CreateEmptyUser(){
        return {
            id:undefined,
            name:'',
            email:'',
            password:0,
            registration:0,
            bond:'',
            birthday:0,
            newslist:0
        };
    }
    _CreateEmptyCompany(){
        return {
            match_id:undefined,
            user_id:undefined,
            company_name:'',
            currency:0,
            marketing_level:0,
            infrastructure_level:0,
            rh_level:0,
            stock:0,
            resource:0
        };
    }
    _CreateEmptyRound(){
        return {
            match_id:undefined,
            user_id:undefined,
            round_num:undefined,
            marketing_evolution:0,
            infrastructure_evolution:0,
            rh_evolution:0,
            purchased_resources:0,
            products_made:0,
            products_sales:0,
            product_price:0
        };
    }
}
export default Game;