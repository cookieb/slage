import React from 'react';
import $ from 'jquery';

class LoungePage extends React.Component{
    constructor(props){
        super(props);
        this.game = props.game;
        this.state={
            rounds:this.game.previous_rounds
        };
        this.game.AddEventListener('round_list_change',this.handleRoundListChange.bind(this));
    }
    handleRoundListChange(){
        this.setState({
            ...this.state,
            rounds:this.game.previous_rounds
        });
    }
    render(){
        if(this.state.rounds === undefined || this.state.rounds.length===0){
            return (
                <div className="container">
                    <div className="feed resp">
                        <div className="qqcoisa">
                            <h4>Nenhuma rodada foi encerrada ainda</h4>
                            <p>Espere seu professor finalizar a rodada</p>
                        </div>
                    </div>
                </div>
            );
        }
        else{
            let rounds = [];
            this.state.rounds.forEach(function(e,i){
                let frase='';
                if(e.marketing_evolution==1){
                    frase+='Você evoluiu o setor de Marketing. ';
                }
                if(e.infrastructure_evolution==1){
                    frase+='Você evoluiu o setor de infraestrutura. ';
                }
                if(e.rh_evolution==1){
                    frase+='Você evoluiu o setor de RH';
                }
                if(e.purchased_resources>0){
                    frase+='Você comprou '+e.purchased_resources+' recursos. ';
                }
                if(e.products_made>0){
                    frase+='Você fez '+e.products_made+' produtos. ';
                }
                if(e.products_sales>0){
                    frase+='Você vendeu '+e.products_sales+' produtos. ';
                }
                if(e.product_price>0){
                    frase+='Você cobrou R$ '+e.products_sales+' por produto. ';
                }
                rounds.push(
                    <div key={i} className="qqcoisa">
                        <h4>Rodada {e.round_num}</h4>
                        <p>{frase}</p>
                    </div>
                );
            });
            return (
                <div className="container">
                    <div className="feed resp">
                        {rounds}
                    </div>
                </div>
            );
        }
    }
}

export default LoungePage;