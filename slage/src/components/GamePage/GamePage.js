import React from 'react';
import $ from 'jquery';
import "../../resources/css/style_salao.css";
import NavBar from '../NavBar/NavBar';
import MenuBar from '../MenuBar/MenuBar';
import GameContentContainer from '../GameContentContainer/GameContentContainer';

class GamePage extends React.Component{
    constructor(props){
        super(props);
        this.game = this.props.game;
    }
    render(){

        return (
            <div>                
                <NavBar game={this.game}/>
                <GameContentContainer game={this.game}/>
                <MenuBar game={this.game}/>
            </div>
        );
    }
}

export default GamePage;