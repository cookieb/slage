<?php 

	header('Access-Control-Allow-Origin: *');

	define('SECRET_KEY', 'your-256-bit-secret');
	// Configuração
	$config['site_url'] = 'https://fases.ifrn.edu.br/slage/slapi/index.php';
	$config['base_url'] = 'https://fases.ifrn.edu.br/slage/';
	$config['database_host'] = 'localhost';
	$config['database_user'] = 'root';
	$config['database_pass'] = '';
	$config['database_name'] = 'slage';
	// Funções
	function base64_url_encode($input) {
		return str_replace(['+','/','='],['-','_',''],base64_encode($input));
	}
	function base64_url_decode($input) {
		return base64_decode(str_replace(['-','_'],['+','/'],$input));
	}
	function CreateToken($header=array(),$content=array()){
		$header_token = base64_url_encode(json_encode($header));
		$content_token = base64_url_encode(json_encode($content));

		$alg='sha256';
		if (isset($header['alg']) && $header['alg']=='HS256') {
			$alg='sha256';
		}

		$signature_token = base64_url_encode(hash_hmac($alg, $header_token.'.'.$content_token, SECRET_KEY,true));

		$token = $header_token . '.' . $content_token . '.' . $signature_token;
		return $token;
	}
	function OpenToken($token){
		if (strpos($token, 'JWT ')!==false) {
			$token = str_replace('JWT ', "", $token);
			if (substr_count($token, '.')==2) {
				$payload = explode('.',substr($token, 0, strrpos($token,'.')));
				$signature = substr($token, strrpos($token, '.')+1);
				$header = json_decode(base64_url_decode($payload[0]),true);
				$content = json_decode(base64_url_decode($payload[1]),true);

				$alg='sha256';
				if (isset($header['alg'])) {
					if ($header['alg']=='HS256') {
						$alg='sha256';
					}
				}

				if ($signature == base64_url_encode(hash_hmac($alg, $payload[0].'.'.$payload[1], SECRET_KEY,true))) {
					return array(
						'header'=>$header,
						'content'=>$content,
						'signature'=>$signature
					);
				}
				else{
					return 3;
				}
			}
			else{
				return 2;
			}
		}
		else{
			return 1;
		}
	}

	//var_dump($_POST);

	// Inicialização
	$routes = array();
	$db = mysqli_connect($config['database_host'],$config['database_user'],$config['database_pass'],$config['database_name']);
	$json = array();
	$token = null;
	if(isset($_POST['commands'])){
		$json = json_decode(utf8_decode($_POST['commands']),true);
	}
	if(isset($json['token'])){
		$token = OpenToken('JWT '.$json['token']);
		if (gettype($token)!=='array') {
			$token=null;
		}
	}
	// Rotas
	$routes['user/create'] = function($db, $token, $request){
		$response = array();
		$response['status']=500;
		if (isset($request['user']) && isset($request['email']) && isset($request['password']) && isset($request['bond'])) {
			$user = $request['user'];
			$email = $request['email'];
			$password = password_hash($request['password'],PASSWORD_DEFAULT);
			$bond = $request['bond'];
			$result  = $db->query('INSERT INTO `slage_users`(`name`, `email`, `password`, `bond`) VALUES (\''.$user.'\',\''.$email.'\',\''.$password.'\',\''.$bond.'\');');
			if($result === false){
				$response['status']=204;
			}
			else{
				$response['status']=200;
			}
		}
		else{
			$response['status']=400;
		}
		return $response;
	};
	$routes['auth/token'] = function($db, $token, $request){
		$response = array();
		$response['status']=200;
		
		if (isset($request['email']) && isset($request['password'])) {
			$email = $request['email'];
			$password = $request['password'];

			$result  = $db->query('SELECT `id`, `name`, `email`, `password`, `bond` FROM `slage_users` WHERE `email`=\''.$email.'\';');
			if($result!==false && $result->num_rows===1){
				$response['status']=401;
				$row = $result->fetch_assoc();
				if(password_verify($password,$row['password'])){
					$response['status']=200;
					$header = array();
					$content = array(
						'id'=>$row['id'],
						'name'=>$row['name'],
						'email'=>$row['email'],
						'bond'=>$row['bond']
					);
					$response['token']=CreateToken($header,$content);
				}
			}
			else{
				$response['status']=404;
			}
		}
		else{
			$response['status']=400;
		}
		return $response;
	};
	$routes['match/list'] = function($db, $token, $request){
		$response = array();
		$response['status']=500;
		if (!is_null($token)) {
			$result  = $db->query('SELECT * FROM `slage_matches`;');
			if($result!==false){
				if($result->num_rows===0){
					$response['matchs']=[];
					$response['status']=204;
				}
				else{
					$response['matchs'] = $result->fetch_all(MYSQLI_ASSOC);
					$response['status']=200;
				}
			}
		}
		else{
			$response['status']=401;
		}
		return $response;
	};
	$routes['match/join'] = function($db, $token, $request){
		$response = array();
		$response['status']=500;
		if (!is_null($token)) {
			if(isset($request['match_id'])){
				$user_id = $token['content']['id'];
				$match_id = $request['match_id'];
				$company_name = $token['content']['name'].'_f';
				$result  = $db->query('INSERT INTO `slage_match_user`(`user_id`, `match_id`, `company_name`) VALUES (\''.$user_id.'\',\''.$match_id.'\',\''.$company_name.'\') ON DUPLICATE KEY UPDATE `stock`=`stock`;');
				if($result!==false){
					$response['status']=200;
					$response['match_id'] = $match_id;
				}
				else{
					$response['status']=404;
				}
			}
			else{
				$response['status']=400;
			}
		}
		else{
			$response['status']=401;
		}
		return $response;
	};
	$routes['match/get'] = function($db, $token, $request){
		$response = array();
		$response['status']=500;
		if (!is_null($token)) {
			if(isset($request['match_id'])){
				$match_id = $request['match_id'];
				$result  = $db->query('SELECT `id`, `owner_id`, `name`, `round_open`, `current_round`, `code` FROM `slage_matches` WHERE `id`=\''.$match_id.'\';');
				if($result!==false){
					$response['status']=200;
					$row = $result->fetch_assoc();
					$response['match'] = $row;
				}
				else{
					$response['status']=404;
				}
			}
			else{
				$response['status']=400;
			}
		}
		else{
			$response['status']=401;
		}
		return $response;
	};
	$routes['company/get'] = function($db, $token, $request){
		$response = array();
		$response['status']=500;
		if (!is_null($token)) {
			if(isset($request['match_id'])){
				$match_id = $request['match_id'];
				$user_id = $request['user_id'];
				$result  = $db->query('SELECT `user_id`, `match_id`, `company_name`, `currency`, `marketing_level`, `infrastructure_level`, `rh_level`, `stock`, `resource` FROM `slage_match_user` WHERE `match_id`=\''.$match_id.'\' AND `user_id`=\''.$user_id.'\';');
				if($result->num_rows===1){
					$response['status']=200;
					$row = $result->fetch_assoc();
					$response['company'] = $row;
				}
				else{
					$response['status']=404;
				}
			}
			else{
				$response['status']=400;
			}
		}
		else{
			$response['status']=401;
		}
		return $response;
	};
	$routes['company/list'] = function($db, $token, $request){
		$response = array();
		$response['status']=500;
		if (!is_null($token)) {
			if(isset($request['match_id'])){
				$match_id = $request['match_id'];
				$result  = $db->query('SELECT `user_id`, `match_id`, `company_name`, `currency`, `marketing_level`, `infrastructure_level`, `rh_level`, `stock`, `resource` FROM `slage_match_user` WHERE `match_id`=\''.$match_id.'\';');
				if($result->num_rows>0){
					$response['status']=200;
					$lines = $result->fetch_all(MYSQLI_ASSOC);
					$response['companies'] = $lines;
				}
				else{
					$response['status']=404;
				}
			}
			else{
				$response['status']=400;
			}
		}
		else{
			$response['status']=401;
		}
		return $response;
	};
	$routes['round/all'] = function($db, $token, $request){
		$response = array();
		$response['status']=500;
		if (!is_null($token)) {
			if(isset($request['match_id'])&&isset($request['user_id'])){
				$match_id = $request['match_id'];
				$user_id = $request['user_id'];

				$result  = $db->query('SELECT `match_id`, `user_id`, `round_num`, `marketing_evolution`, `infrastructure_evolution`, `rh_evolution`, `purchased_resources`, `products_made`, `products_sales`, `product_price` FROM `slage_rounds` WHERE `user_id`=\''.$user_id.'\' AND `match_id`=\''.$match_id.'\' ORDER BY `round_num` DESC;');

				if($result->num_rows>0){
					$response['status']=200;
					$lines = $result->fetch_all(MYSQLI_ASSOC);
					$response['rounds'] = $lines;
				}
				else{
					$response['status']=404;
				}
			}
			else{
				$response['status']=400;
			}
		}
		else{
			$response['status']=401;
		}
		return $response;
	};
	$routes['round/send'] = function($db, $token, $request){
		$response = array();
		$response['status']=500;
		if (!is_null($token)) {
			if(isset($request['round'])){
				$round = 					$request['round'];
				$match_id = 				$round['match_id'];
				$user_id = 					$round['user_id'];
				$round_num = 				$round['round_num'];
				$marketing_evolution = 		$round['marketing_evolution'];
				$infrastructure_evolution = $round['infrastructure_evolution'];
				$rh_evolution = 			$round['rh_evolution'];
				$purchased_resources = 		$round['purchased_resources'];
				$products_made = 			$round['products_made'];
				$products_sales = 			$round['products_sales'];
				$product_price = 			$round['product_price'];

				$result = $db->query('SELECT * FROM `slage_matches` WHERE `id`=\''.$match_id.'\';');

				$response['status']=404;
				
				if($result!== false && $result->num_rows===1){
					$row = $result->fetch_assoc();
					if ($round_num === $row['current_round']) {
						$result = $db->query('INSERT INTO `slage_rounds`(`match_id`, `user_id`, `round_num`, `marketing_evolution`, `infrastructure_evolution`, `rh_evolution`, `purchased_resources`, `products_made`, `products_sales`, `product_price`) VALUES (\''.$match_id.'\',\''.$user_id.'\',\''.$round_num.'\',\''.$marketing_evolution.'\',\''.$infrastructure_evolution.'\',\''.$rh_evolution.'\',\''.$purchased_resources.'\',\''.$products_made.'\',\''.$products_sales.'\',\''.$product_price.'\') ON DUPLICATE KEY UPDATE `match_id`=\''.$match_id.'\',`user_id`=\''.$user_id.'\',`round_num`=\''.$round_num.'\',`marketing_evolution`=\''.$marketing_evolution.'\',`infrastructure_evolution`=\''.$infrastructure_evolution.'\',`rh_evolution`=\''.$rh_evolution.'\',`purchased_resources`=\''.$purchased_resources.'\',`products_made`=\''.$products_made.'\',`products_sales`=\''.$products_sales.'\',`product_price`=\''.$product_price.'\';');
						if($result!==false){
							$response['status']=200;
						}	
					}
				}
			}
			else{
				$response['status']=400;
			}
		}
		else{
			$response['status']=401;
		}
		return $response;
	};
	$routes['round/close'] = function($db, $token, $request){
		$response = array();
		$response['status']=500;
		if (!is_null($token)) {
			if(isset($request['match_id'])){
				$match_id = $request['match_id'];
				$response['status']=404;

				$result  = $db->query('SELECT `id`, `owner_id`, `name`, `round_open`, `current_round`, `code` FROM `slage_matches` WHERE `id`=\''.$match_id.'\';');

				if($result->num_rows==1){
					$match = $result->fetch_assoc();
					$result = $db->query('SELECT `user_id`, `match_id`, `company_name`, `currency`, `marketing_level`, `infrastructure_level`, `rh_level`, `stock`, `resource` FROM `slage_match_user` WHERE `match_id`=\''.$match['id'].'\';');
					$companies = $result->fetch_all(MYSQLI_ASSOC);
					$result = $db->query('SELECT `match_id`, `user_id`, `round_num`, `marketing_evolution`, `infrastructure_evolution`, `rh_evolution`, `purchased_resources`, `products_made`, `products_sales`, `product_price` FROM `slage_rounds` WHERE `match_id`=\''.$match['id'].'\' AND `round_num`=\''.$match['current_round'].'\';');
					$rounds = $result->fetch_all(MYSQLI_ASSOC);

					$pair = [];

					$len_rounds = count($rounds);
					$len_companies = count($companies);

					for ($i=0; $i < $len_rounds; $i++) { 
						for ($j=0; $j < $len_companies; $j++) { 
							if ($companies[$j]['user_id']==$rounds[$i]['user_id']) {
								array_push($pair, array(
									'round'=>$rounds[$i],
									'company'=>$companies[$j]
								));	
							}
						}
					}
					$len_pair = count($pair);
					$querys = '';
					for ($i=0; $i < $len_pair; $i++) {
						$expenses = 0;
						$profit = 0;
						if ($pair[$i]['round']['marketing_evolution']==1) {
							$expenses += 100;
							$pair[$i]['company']['marketing_level']+=1;
						}
						if ($pair[$i]['round']['infrastructure_evolution']==1) {
							$expenses += 100;
							$pair[$i]['company']['infrastructure_level']+=1;
						}
						if ($pair[$i]['round']['rh_evolution']==1) {
							$expenses += 100;
							$pair[$i]['company']['rh_level']+=1;
						}
						$expenses += $pair[$i]['round']['purchased_resources'] * 1;
						$expenses += $pair[$i]['round']['products_made'] * 1;
						$profit += $pair[$i]['round']['products_sales'] * $pair[$i]['round']['product_price'];
						if(
							$pair[$i]['company']['currency']+$profit >= $expenses &&
							$pair[$i]['company']['resource'] >= $pair[$i]['round']['products_made'] &&
							$pair[$i]['company']['stock'] >= $pair[$i]['round']['products_sales']
						){
							$pair[$i]['company']['currency']+=$profit-$expenses;
							$pair[$i]['company']['stock']+=$pair[$i]['round']['products_made']-$pair[$i]['round']['products_sales'];
							$pair[$i]['company']['resource']+=$pair[$i]['round']['purchased_resources']-$pair[$i]['round']['products_made'];
							$querys.='UPDATE `slage_match_user` SET `currency`=\''.$pair[$i]['company']['currency'].'\',`marketing_level`=\''.$pair[$i]['company']['marketing_level'].'\',`infrastructure_level`=\''.$pair[$i]['company']['infrastructure_level'].'\',`rh_level`=\''.$pair[$i]['company']['rh_level'].'\',`stock`=\''.$pair[$i]['company']['stock'].'\',`resource`=\''.$pair[$i]['company']['resource'].'\' WHERE `user_id`=\''.$pair[$i]['company']['user_id'].'\' AND `match_id`=\''.$pair[$i]['company']['match_id'].'\';';
						}
						else{
							$querys.='UPDATE `slage_rounds` SET `marketing_evolution`=\'0\',`infrastructure_evolution`=\'0\',`rh_evolution`=\'0\',`purchased_resources`=\'0\',`products_made`=\'0\',`products_sales`=\'0\',`product_price`=\'0\' WHERE `match_id`=\''.$pair[$i]['round']['match_id'].'\' AND `user_id`=\''.$pair[$i]['round']['user_id'].'\' AND `round_num`==\''.$pair[$i]['round']['round_num'].'\';';
						}
					}
					$querys='UPDATE `slage_matches` SET `current_round`=\''.($match['current_round']+1).'\' WHERE `id`=\''.$match['id'].'\';'.$querys;
					$db->multi_query($querys);
					$response['status']=200;
				}
			}
			else{
				$response['status']=400;
			}
		}
		else{
			$response['status']=401;
		}
		return $response;
	};
	/*
	$routes[''] = function($request,$response,$status_code){

	};
	*/

	// Retorno
	$routes_names = array_keys($routes);
	$response = array('status'=>200);
	if(isset($json['commands'])){
		$response['commands'] = array();
		foreach ($json['commands'] as $command => $arguments) {
			if(array_search($command, $routes_names) !== false){
				$result = $routes[$command]($db, $token, $arguments);
				$response['commands'][$command] = $result;
			}
		}
	}
	echo utf8_encode(json_encode($response));
?>